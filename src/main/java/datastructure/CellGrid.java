package datastructure;

import cellular.CellState;

public class CellGrid implements IGrid {

    int rows, cols;
    cellular.CellState[][] Cellgrid;

    //List<CellState> CellState = new ArrayList<CellState>(rows * cols);

    public CellGrid(int rows, int columns, CellState initialState) {
        this.rows = rows;
        this.cols = columns;
        this.Cellgrid = new CellState[rows][columns];

        for (int x = 0; x<rows; x++){
            for (int y = 0; y<columns; y++){
                this.Cellgrid[x][y] = initialState;
            }
        }
        // TODO Auto-generated constructor stub
    }

    @Override
    public int numRows() {
        // TODO Auto-generated method stub
        return this.rows;
    }

    @Override
    public int numColumns() {
        // TODO Auto-generated method stub
        return this.cols;
    }

    @Override
    public void set(int row, int column, CellState element) {
        // TODO Auto-generated method stub
        try {Cellgrid[row][column] = element;
        }
        catch (IndexOutOfBoundsException e){
            throw new IndexOutOfBoundsException();
        }
    }

    @Override
    public CellState get(int row, int column) {
        // TODO Auto-generated method stub
        try {return Cellgrid[row][column];
        }
        catch (IndexOutOfBoundsException e){
            throw new IndexOutOfBoundsException();
        }
    }


    @Override
    public IGrid copy() {
        IGrid copy = new CellGrid(this.rows, this.cols, cellular.CellState.DEAD);
        for (int x = 0; x <this.rows; x++){
            for (int y = 0; y <this.cols; y++){
                copy.set(x, y, get(x, y));
            }
        }
        // TODO Auto-generated method stub
        return copy;
    }

    /*private int coordinateConverter(int x, int y) {
        return x + numRows()*y;
    }*/
}
