package cellular;

import java.lang.reflect.Array;
import java.util.Random;

import datastructure.CellGrid;
import datastructure.IGrid;

import datastructure.IGrid;

public class BriansBrain implements CellAutomaton {

    /**
     * The grid of cells
     */
    IGrid currentGeneration;

    /**
     * Construct a Game Of Life Cell Automaton that holds cells in a grid of the
     * provided size
     *
     * @param height The height of the grid of cells
     * @param width  The width of the grid of cells
     */
    public BriansBrain(int rows, int columns) {
        currentGeneration = new CellGrid(rows, columns, CellState.DEAD);
        initializeCells();
    }

    @Override
    public void initializeCells() {
        Random random = new Random();
        for (int row = 0; row < currentGeneration.numRows(); row++) {
            for (int col = 0; col < currentGeneration.numColumns(); col++) {
                if (random.nextBoolean()) {
                    currentGeneration.set(row, col, CellState.ALIVE);
                } else {
                    currentGeneration.set(row, col, CellState.DEAD);
                }
            }
        }
    }

    @Override
    public int numberOfRows() {
        // TODO
        return currentGeneration.numRows();
    }

    @Override
    public int numberOfColumns() {
        // TODO
        return currentGeneration.numColumns();
    }

    @Override
    public CellState getCellState(int row, int col) {
        CellState state = currentGeneration.get(row, col);
        // TODO
        return state;
    }

    @Override
    public void step() {
        IGrid nextGeneration = currentGeneration.copy();
        // TODO
        for (int row = 0; row < currentGeneration.numRows(); row++) {
            for (int col = 0; col < currentGeneration.numColumns(); col++) {
                nextGeneration.set(row, col, getNextCell(row, col));
            }
        }
        currentGeneration = nextGeneration;
    }

    @Override
    public CellState getNextCell(int row, int col) {
        CellState cell = getCellState(row, col);

        /*if (cell == CellState.ALIVE) {
            if (countNeighbors(row, col, CellState.ALIVE) == 2 || countNeighbors(row, col, CellState.ALIVE) == 3) {
                return CellState.ALIVE;
            }
            if (countNeighbors(row, col, CellState.ALIVE) < 2 || countNeighbors(row, col, CellState.ALIVE) > 3) {
                return CellState.DEAD;
            }
        }
        if (cell == CellState.DEAD && countNeighbors(row, col, CellState.ALIVE) == 3) {
            return CellState.DEAD;
        }*/
        if (cell == CellState.ALIVE){
            return CellState.DYING;
        }
        if (cell == CellState.DYING){
            return CellState.DEAD;
        }
        if (cell == CellState.DEAD && countNeighbors(row, col, CellState.ALIVE) == 2){
            return CellState.ALIVE;
        }
        return cell;
    }
        /*CellState cell = getCellState(row, col);
        if (cell == CellState.ALIVE) {
            if ((countNeighbors(row, col, CellState.ALIVE) == 2 || (countNeighbors(row, col, CellState.ALIVE) == 3))) {
                return CellState.ALIVE;
            } else {
                if (countNeighbors(row, col, CellState.ALIVE) == 3) {
                    return CellState.ALIVE;
                }
            }
        }*/
        //return CellState.DEAD;
        // TODO
    /**
     * Calculates the number of neighbors having a given CellState of a cell on
     * position (row, col) on the board
     *
     * Note that a cell has 8 neighbors in total, of which any number between 0 and
     * 8 can be the given CellState. The exception are cells along the boarders of
     * the board: these cells have anywhere between 3 neighbors (in the case of a
     * corner-cell) and 5 neighbors in total.
     *
     * @param x     the x-position of the cell
     * @param y     the y-position of the cell
     * @param state the Cellstate we want to count occurences of.
     * @return the number of neighbors with given state
     */
    private int countNeighbors(int row, int col, CellState state) {
        int count = 0;
        for (int x = col-1; x <= row+1; x++) {
            for (int y = row -1; y < row+1; y++) {
                if (x > currentGeneration.numColumns() || x < 0 || y > currentGeneration.numRows() || y < 0){
                    continue;
                }
                if (x == col && y == row) {
                    continue;
                }
                else if (currentGeneration.get(y, x) == state) {
                    count += 1;
                }
            }
        }
        // TODO
        return count;
    }

    @Override
    public IGrid getGrid() {
        return currentGeneration;
    }
}